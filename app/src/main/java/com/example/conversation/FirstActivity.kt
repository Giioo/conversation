package com.example.conversation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {

    private val requestCodeAddress: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }

    private fun init() {
        openSecondActivityButton.setOnClickListener {
            openActivity()
        }
    }

    private fun openActivity() {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("first_name", firstNameET.text.toString())
        intent.putExtra("last_name", lastNameET.text.toString())
        intent.putExtra("age", ageET.text.toString().toInt())
        startActivityForResult(intent, requestCodeAddress)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == requestCodeAddress && resultCode == RESULT_OK) {
            val address = data?.extras?.getString("address", "")
            addressET.setText(address)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}