package com.example.conversation

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
        val firstName = intent.extras?.getString("first_name", "")
        val lastName = intent.extras?.getString("last_name", "")
        val age = intent.extras?.getInt("age", 0)

        firstNameTV.text = firstName
        lastNameTV.text = lastName
        ageTV.text = age.toString()

        returnAddressButton.setOnClickListener {
            returnAddress()
        }
    }
    private fun returnAddress(){
        val intentOb = intent
        intentOb.putExtra("address",addressET.text.toString())
        setResult(Activity.RESULT_OK, intentOb)
        finish()
    }
}